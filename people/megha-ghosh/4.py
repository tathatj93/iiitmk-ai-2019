def assignment_4(look) -> bool:
    right = look("right")
    while not (right[0][0] is None and right[1][2] is None and right[2][6] is None):
        right = look("right")
    return True
